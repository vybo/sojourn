//
//  PlaceDetailController.m
//  Sojourn
//
//  Created by Dan Vybíral on 17/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import "PlaceDetailController.h"
#import "PlacePhotoController.h"
#import "ManagedPlace.h"
#import <LBBlurredImage/UIImageView+LBBlurredImage.h>
@import MapKit;

@interface PlaceDetailController ()

@property (assign, nonatomic) BOOL didEdit;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation PlaceDetailController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newManagedPlace {
    if (_managedPlace != newManagedPlace) {
        _managedPlace = newManagedPlace;
        
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.managedPlace) {
        ManagedPlace *place = (ManagedPlace *) _managedPlace;
        
        _placeTitle.text = place.placeName;
        _placeDescription.text = place.placeDescription;
        _placeDescription.backgroundColor = [UIColor clearColor];
        _placeTime.text = [_dateFormatter stringFromDate:place.placeDate];
        _placeImage.image = [UIImage imageWithData:place.placeImage];
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([place.placeLocationLat doubleValue], [place.placeLocationLon doubleValue]);
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 800, 800);
        [self.placeMap setRegion:[self.placeMap regionThatFits:region] animated:YES];
        
        // Add an annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = coord;
        point.title = place.placeName;
        point.subtitle = @"You were standing right here.";
        
        [self.placeMap addAnnotation:point];

    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm"];
    
    //[self loadPlace:_place];
    [self configureView];
    
    // Rounded image with border
    CALayer *imageLayer = [_placeImage layer];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setCornerRadius:50.0];
    
    if (_placeImage.image == nil) {
        [imageLayer setBorderWidth:0.5];
        [imageLayer setBorderColor:[[UIColor grayColor] CGColor]];
    }
    
    _didEdit = NO;
    
    // Blurred Background
    
    // 1
    
    if (_placeImage.image != nil){
        _screenHeight = [UIScreen mainScreen].bounds.size.height;
        /*
        NSString *imageName = [NSString stringWithFormat:@"%li.jpg", (long)[self randomNumberBetween:1 maxNumber:5]];
         */
        UIImage *background = _placeImage.image;
    
        // 2
    
        _backgroundImageView = [[UIImageView alloc] initWithImage:background];
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImageView.alpha = 0.8;
        [_backgroundImageView setImageToBlur:background blurRadius:10 completionBlock:nil];
    
        [self.view addSubview:_backgroundImageView];
        [self.view sendSubviewToBack:_backgroundImageView];
    
        _detailView.backgroundColor = [UIColor clearColor];
    
        _placeDescription.textColor = [UIColor whiteColor];
        _placeTitle.textColor = [UIColor whiteColor];
        _placeTime.textColor = [UIColor whiteColor];
        _placeImage.alpha = 1;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)randomNumberBetween:(NSInteger)min maxNumber:(NSInteger)max
{
    return min + arc4random_uniform(max - min + 1);
}

- (void) loadPlace:(Place *)which {
    _placeTitle.text = which.placeName;
    _placeDescription.text = which.placeDescription;
    _placeTime.text = [_dateFormatter stringFromDate:which.placeDatetime];
    _placeImage.image = which.placeImage;
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(which.placeLocation.coordinate, 800, 800);
    [self.placeMap setRegion:[self.placeMap regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = which.placeLocation.coordinate;
    point.title = which.placeName;
    point.subtitle = @"You were standing right here.";
    
    [self.placeMap addAnnotation:point];
}

- (void)newPlaceControllerDelegateDidCancel:(NewPlaceController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)newPlaceControllerDelegateDidSave:(NewPlaceController *)controller didAddPlace:(Place *)newPlace
{
    /*
    _place = newPlace;
    
    _place.placeName = newPlace.placeName;
    _place.placeDescription = newPlace.placeDescription;
    _place.placeDatetime = newPlace.placeDatetime;
    _place.placeLocation = newPlace.placeLocation;
    _place.placeImage = newPlace.placeImage;
    
    [self loadPlace:_place];
    _didEdit = YES;
     */
    
    ManagedPlace *place = (ManagedPlace *) _managedPlace;
    
    place.placeName = controller.placeNameField.text;
    place.placeDescription = controller.placeDescriptionField.text;
    place.placeDate = controller.placeTime;
    place.placeLocationLat = [NSNumber numberWithDouble:controller.placeLocation.coordinate.latitude];
    place.placeLocationLon = [NSNumber numberWithDouble:controller.placeLocation.coordinate.longitude];
    place.placeImage = UIImagePNGRepresentation(controller.placePhoto.image);
    
    [self configureView];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)newPlaceControllerDelegateProvideManagedPlaceToEdit{
    return _managedPlace;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EditPlace"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        NewPlaceController *newPlaceViewController = [navigationController viewControllers][0];
        newPlaceViewController.delegate = self;
        //newPlaceViewController.place = _place;
        ManagedPlace *place = (ManagedPlace *) _managedPlace;
        
        newPlaceViewController.placeNameField.text = place.placeName;
        newPlaceViewController.placeDescriptionField.text = place.placeDescription;
        newPlaceViewController.placeTimeVisited.text = [_dateFormatter stringFromDate:place.placeDate];
        newPlaceViewController.placeTime = place.placeDate;
        newPlaceViewController.placeLocation = [[CLLocation alloc] initWithLatitude:[place.placeLocationLat doubleValue] longitude:[place.placeLocationLon doubleValue]];
        newPlaceViewController.placePhoto.image = [UIImage imageWithData:place.placeImage];
    }
    else if ([segue.identifier isEqualToString:@"placePhoto"]) {
        PlacePhotoController *placePhotoController = segue.destinationViewController;
        placePhotoController.placeImage = _placeImage.image;
    }
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (![parent isEqual:self.parentViewController]) {
        if (_didEdit) {
            [self.delegate placeDetailcontrollerDelegateDidEdit:self editedPlace:_place];
        } else {
            [self.delegate placeDetailControllerDelegateDidClose:self];
        }
    }
}

@end
