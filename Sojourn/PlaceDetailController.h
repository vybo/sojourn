//
//  PlaceDetailController.h
//  Sojourn
//
//  Created by Dan Vybíral on 17/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "NewPlaceController.h"

@class PlaceDetailController;

@protocol PlaceDetailControllerDelegate <NSObject>

- (void)placeDetailControllerDelegateDidClose:(PlaceDetailController *)controller;
- (void)placeDetailcontrollerDelegateDidEdit:(PlaceDetailController *)controller editedPlace: (Place *) editedPlace;

@end

@interface PlaceDetailController : UIViewController <NewPlaceControllerDelegate>

@property (nonatomic, weak) id <PlaceDetailControllerDelegate> delegate;
@property (weak, nonatomic) Place *place;
@property (strong, nonatomic) id managedPlace;

@property (weak, nonatomic) IBOutlet UILabel *placeTitle;
@property (weak, nonatomic) IBOutlet UITextView *placeDescription;
@property (weak, nonatomic) IBOutlet UILabel *placeTime;
@property (weak, nonatomic) IBOutlet MKMapView *placeMap;
@property (weak, nonatomic) IBOutlet UIImageView *placeImage;

@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, assign) CGFloat screenHeight;

@property (strong, nonatomic) IBOutlet UIView *detailView;

- (void) loadPlace: (Place *)which;

@end
