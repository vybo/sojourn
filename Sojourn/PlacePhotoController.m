//
//  PlacePhotoController.m
//  Sojourn
//
//  Created by student on 30/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import "PlacePhotoController.h"

@interface PlacePhotoController ()

@end

@implementation PlacePhotoController

- (void)viewDidLoad {
    [super viewDidLoad];
    _imageView.image = _placeImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillLayoutSubviews{
    self.scrollView.contentSize = self.imageView.image.size;
    self.imageView.frame = CGRectMake(0, 0, self.imageView.image.size.width, self.imageView.image.size.height);
    
    self.scrollView.minimumZoomScale = 0;
    self.scrollView.maximumZoomScale = 1;
    self.scrollView.zoomScale = 0;
    self.scrollView.backgroundColor = [UIColor blackColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
