//
//  ManagedPlace.h
//  Sojourn
//
//  Created by Dan Vybíral on 25/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ManagedPlace : NSManagedObject

@property (nonatomic, retain) NSString * placeName;
@property (nonatomic, retain) NSString * placeDescription;
@property (nonatomic, retain) NSDate * placeDate;
@property (nonatomic, retain) NSData * placeImage;
@property (nonatomic, retain) NSNumber * placeLocationLat;
@property (nonatomic, retain) NSNumber * placeLocationLon;

@end
