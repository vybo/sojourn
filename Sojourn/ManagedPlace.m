//
//  ManagedPlace.m
//  Sojourn
//
//  Created by Dan Vybíral on 25/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import "ManagedPlace.h"


@implementation ManagedPlace

@dynamic placeName;
@dynamic placeDescription;
@dynamic placeDate;
@dynamic placeImage;
@dynamic placeLocationLat;
@dynamic placeLocationLon;

@end
