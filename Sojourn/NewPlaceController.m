//
//  NewPlaceController.m
//  Sojourn
//
//  Created by Dan Vybíral on 17/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <FlickrKit.h>
#import "NewPlaceController.h"
#import "Place.h"
#import "ManagedPlace.h"


@interface NewPlaceController () <UITextFieldDelegate, CLLocationManagerDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *placeLocationMap;
@property (strong, nonatomic) CLPlacemark *placePlacemark;
@property (strong, nonatomic) UIImage *flickrImage;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, retain) FKFlickrNetworkOperation *myPhotostreamOp;

@end

@implementation NewPlaceController

- (void) dealloc {
    [self.myPhotostreamOp cancel];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Textfield borders
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, _placeNameField.frame.size.height - borderWidth, _placeNameField.frame.size.width, _placeNameField.frame.size.height);
    border.borderWidth = borderWidth;
    [_placeNameField.layer addSublayer:border];
    _placeNameField.layer.masksToBounds = YES;
    
    CALayer *border2 = [CALayer layer];
    border2.borderColor = [UIColor lightGrayColor].CGColor;
    border2.frame = CGRectMake(0, _placeDescriptionField.frame.size.height - borderWidth, _placeDescriptionField.frame.size.width, _placeDescriptionField.frame.size.height);
    border2.borderWidth = borderWidth;
    [_placeDescriptionField.layer addSublayer:border2];
    _placeDescriptionField.layer.masksToBounds = YES;
    
    CALayer *border3 = [CALayer layer];
    border3.borderColor = [UIColor lightGrayColor].CGColor;
    border3.frame = CGRectMake(0, _placeTimeVisited.frame.size.height - borderWidth, _placeTimeVisited.frame.size.width, _placeTimeVisited.frame.size.height);
    border3.borderWidth = borderWidth;
    [_placeTimeVisited.layer addSublayer:border3];
    _placeTimeVisited.layer.masksToBounds = YES;
    
    // Rounded image with border
    CALayer *imageLayer = [_placePhoto layer];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setCornerRadius:50.0];
    
    [imageLayer setBorderWidth:0.5];
    [imageLayer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    
    //Datetime picker and textfield
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm"];
    
    //Assign from older place
    if ([_delegate respondsToSelector:@selector(newPlaceControllerDelegateProvideManagedPlaceToEdit)]) {
        ManagedPlace *place = [_delegate newPlaceControllerDelegateProvideManagedPlaceToEdit];
        _placeNameField.text = place.placeName;
        _placeDescriptionField.text = place.placeDescription;
        _placeTimeVisited.text = [_dateFormatter stringFromDate:place.placeDate];
        _placePhoto.image = [UIImage imageWithData:place.placeImage];
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([place.placeLocationLat doubleValue], [place.placeLocationLon doubleValue]);
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 800, 800);
        [self.placeLocationMap setRegion:[self.placeLocationMap regionThatFits:region] animated:YES];
        
        // Add an annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = coord;
        point.title = @"Your old location";
        
        [self.placeLocationMap addAnnotation:point];

    }
    
    if (_place != nil) {
        _placeTime = _place.placeDatetime;
        _placeTimeVisited.text = [_dateFormatter stringFromDate:_placeTime];
        _placeNameField.text = _place.placeName;
        _placeDescriptionField.text= _place.placeDescription;
        _placePhoto.image = _place.placeImage;
        _placeLocation = _place.placeLocation;
        
    } else {
        _placeTime = [NSDate date]; //////// !!!!!!!!!!!!!
        _placeLocation = [[CLLocation alloc] init];

    }
    
    _placeTimeVisited.placeholder = [_dateFormatter stringFromDate:_placeTime];
    
    _datePicker = [[UIDatePicker alloc] init];
    
    [_datePicker addTarget:self action:@selector(datePicked) forControlEvents:UIControlEventValueChanged];
    
    _placeTimeVisited.inputView = _datePicker;
    
    //Location manager
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _placeLocationMap.delegate = self;
    _locationManager.delegate = self;
    
    MKUserLocation *userLocation = _placeLocationMap.userLocation;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance
    (userLocation.location.coordinate,
     20000, 20000);
    [_placeLocationMap setRegion:region animated:YES];
    
    // Long press recognizer
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [self.placeLocationMap addGestureRecognizer:longPressGesture];
    
    //Flickr for photos
    [[FlickrKit sharedFlickrKit] initializeWithAPIKey:@"847c31edd84e17eeaa5f3efa5230cf0b" sharedSecret:@"cf82c0b3d388bc56"]; // Shhhhh it's my secret
}

-(void)handleLongPressGesture:(UIGestureRecognizer*)sender {
   
    for (id<MKAnnotation> annot in _placeLocationMap.annotations) {
        [_placeLocationMap removeAnnotation:annot];
    }
    
    CGPoint point = [sender locationInView:self.placeLocationMap];
    CLLocationCoordinate2D locCoord = [self.placeLocationMap convertPoint:point toCoordinateFromView:self.placeLocationMap];
    MKPointAnnotation *dropPin = [[MKPointAnnotation alloc] init];
    dropPin.coordinate = locCoord;
    dropPin.title = @"New location";
    [self.placeLocationMap addAnnotation:dropPin];
   
    _placeLocation = [[CLLocation alloc] initWithLatitude:locCoord.latitude longitude:locCoord.longitude];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation: _placeLocation completionHandler: ^(NSArray *placemarks, NSError *error) {
        _placePlacemark = [placemarks objectAtIndex:0];
    }];
    
    if ([_placeNameField.text isEqualToString:@""]){
        _placeNameField.placeholder = [_placePlacemark subLocality];
    }
    
    if ([_placeDescriptionField.text isEqualToString:@""]){
        [[_placePlacemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
    }
}

- (void)datePicked {
    _placeTime = _datePicker.date;
    _placeTimeVisited.text = [_dateFormatter stringFromDate:_placeTime];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    //Reset time picker and datetime
    if (textField == _placeTimeVisited)
    {
        _placeTime = [NSDate date];
        _placeTimeVisited.placeholder = [_dateFormatter stringFromDate:_placeTime];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Couldn't obtain location.");
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    _placeLocation = locations[0];
    
    MKUserLocation *userLocation = _placeLocationMap.userLocation;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance
    (userLocation.location.coordinate,
     20000, 20000);
    [_placeLocationMap setRegion:region animated:YES];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation: _placeLocation completionHandler: ^(NSArray *placemarks, NSError *error) {
        _placePlacemark = [placemarks objectAtIndex:0];
    }];
    
    if ([_placeNameField.text isEqualToString:@""]){
        _placeNameField.placeholder = [_placePlacemark subLocality];
    }
    
    if ([_placeDescriptionField.text isEqualToString:@""]){
        [[_placePlacemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
    }
    
    [_locationManager stopUpdatingLocation];
}

- (void) getImageFromFlickr {
    NSString *searchTags;
    
    if ([_placeNameField.text isEqualToString:@""]) {
        searchTags = [NSString stringWithFormat:@"%@", _placeNameField.placeholder];
    } else {
        searchTags = [NSString stringWithFormat:@"%@", _placeNameField.text];
    }
    
    FKFlickrPhotosSearch *search = [[FKFlickrPhotosSearch alloc] init];
    search.text = searchTags;
    search.per_page = @"1";
    [[FlickrKit sharedFlickrKit] call:search completion:^(NSDictionary *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (response) {
                NSMutableArray *photoURLs = [NSMutableArray array];
                
                for (NSDictionary *photoDictionary in [response valueForKeyPath:@"photos.photo"]) {
                    NSURL *url = [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeLarge1024 fromPhotoDictionary:photoDictionary]; // Slo by optimalizovat na pouzivani thumbnailu
                    [photoURLs addObject:url];
                }
                
                NSURL *url = [photoURLs objectAtIndex:0];
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    
                    UIImage *image = [[UIImage alloc] initWithData:data];
                    _placePhoto.image = image;
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                }];
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        });
    }];
}

- (void)mapViewWillStartLocatingUser:(MKMapView *)mapView
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if ((status == kCLAuthorizationStatusNotDetermined || status == kCLAuthorizationStatusDenied) && [_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    else if (status == kCLAuthorizationStatusDenied) {
        NSLog(@"Location services denied");
    } else {
        [_locationManager startUpdatingLocation];
    }
}



#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)takeOrPickPhoto:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Do you want to take a new photo or select an existing one?"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take photo", @"Select from gallery", @"Download magically", nil];
    if (_placePhoto.image != nil) {
        [actionSheet setTitle:@"Do you want to take a new photo, select an existing one from gallery or remove it?"];
        [actionSheet addButtonWithTitle:@"Remove photo"];
    }
    
    [actionSheet addButtonWithTitle:@"Cancel"];
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if (buttonIndex == 0){
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera. Select picture from your gallery instead or let the app download some image for you."
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        } else {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        [self presentViewController:picker animated:YES completion:NULL];
    } else if (buttonIndex == 1) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    } else if (buttonIndex == 2) {
        if ([_placeNameField.placeholder isEqualToString:@""] && [_placeNameField.text isEqualToString:@""]) {
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Magic cannot happen!"
                                                                  message:@"Please enter place's name and try again."
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
        } else {
            [self getImageFromFlickr];
        }
    } else if (buttonIndex == 3) {
        _placePhoto.image = nil;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _placePhoto.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)cancel:(id)sender {
    [self.delegate newPlaceControllerDelegateDidCancel:nil];
}

- (IBAction)save:(id)sender {
    Place *newPlace = [[Place alloc] init];
    newPlace.placeDatetime = _placeTime;
    
    if ([_placeDescriptionField.text isEqualToString:@""]) {
        newPlace.placeDescription = [[_placePlacemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
        _placeDescriptionField.text = newPlace.placeDescription;
    } else {
        newPlace.placeDescription = _placeDescriptionField.text;
    }
    
    if ([_placeNameField.text isEqual: @""]) {
        newPlace.placeName = [_placePlacemark subLocality];
        _placeNameField.text = newPlace.placeName;
    } else {
        newPlace.placeName = _placeNameField.text;
    }
    
    if (_placePhoto.image == nil) {
        _placePhoto.image = _flickrImage;
    }
    
    newPlace.placeImage = _placePhoto.image;
    
    newPlace.placeLocation = _placeLocation;
    [self.delegate newPlaceControllerDelegateDidSave:self didAddPlace:newPlace];
}


@end
