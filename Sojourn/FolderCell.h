//
//  FolderCellTableViewCell.h
//  Sojourn
//
//  Created by Dan Vybíral on 17/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FolderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *folderHeader;
@property (weak, nonatomic) IBOutlet UILabel *folderTitle;
@property (weak, nonatomic) IBOutlet UILabel *folderSubtitle;


@end
