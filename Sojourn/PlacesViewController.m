//
//  PlacesViewController.m
//  Sojourn
//
//  Created by Dan Vybíral on 17/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import "PlacesViewController.h"
#import "PlaceDetailController.h"
#import <LBBlurredImage/UIImageView+LBBlurredImage.h>
#import "Place.h"
#import "PlaceCell.h"
#import "ManagedPlace.h"

@interface PlacesViewController ()
@property (strong, nonatomic) IBOutlet UITableView *placesTableView;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (assign, nonatomic) NSInteger selectedIndex;
@end

@implementation PlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm"];
    
    _selectedIndex = -1;
    
    // Blurred Background
    
    // 1
    
    _screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    NSString *imageName = [NSString stringWithFormat:@"%li.jpg", (long)[self randomNumberBetween:1 maxNumber:5]];
    
    UIImage *background = [UIImage imageNamed:imageName];
    
    // 2
    
    _backgroundImageView = [[UIImageView alloc] initWithImage:background];
    _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    _backgroundImageView.alpha = 0.8;
    [_backgroundImageView setImageToBlur:background blurRadius:10 completionBlock:nil];

    [self.view addSubview:_backgroundImageView]; // proste nejde _view WTF
    [self.view sendSubviewToBack:_backgroundImageView];
    
    _placeTableView.backgroundColor = [UIColor clearColor];
    _placeTableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.2];

    /*
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.2];
    self.tableView.pagingEnabled = YES;
    [self.view addSubview:self.tableView];
    */
}

- (NSInteger)randomNumberBetween:(NSInteger)min maxNumber:(NSInteger)max
{
    return min + arc4random_uniform(max - min + 1);
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGRect bounds = self.view.bounds;
    
    _backgroundImageView.frame = bounds;
    //self.tableView.frame = bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)newPlaceControllerDelegateDidCancel:(NewPlaceController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)newPlaceControllerDelegateDidSave:(NewPlaceController *)controller didAddPlace:(Place *)newPlace
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    ManagedPlace *newManagedPlace = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];

    /* KVC
    [newMeasurement setValue:[NSDate date] forKey:@"measurementTime"];
    [newMeasurement setValue:@(1.0) forKey:@"latitude"];
    [newMeasurement setValue:@(2.0) forKey:@"longitude"]; //POZN Jak lip psat settery?
    */
    /*
    [newManagedPlace setValue:controller.placeNameField.text forKey:@"placeName"];
    [newManagedPlace setValue:controller.placeDescriptionField.text forKey:@"placeDescription"];
    [newManagedPlace setValue:controller.placeTime forKey:@"placeDate"];
    */
    
    
    newManagedPlace.placeName = controller.placeNameField.text;
    newManagedPlace.placeDescription = controller.placeDescriptionField.text;
    newManagedPlace.placeDate = controller.placeTime;
    newManagedPlace.placeLocationLat = [NSNumber numberWithDouble:controller.placeLocation.coordinate.latitude];
    newManagedPlace.placeLocationLon = [NSNumber numberWithDouble:controller.placeLocation.coordinate.longitude];
    newManagedPlace.placeImage = UIImagePNGRepresentation(controller.placePhoto.image);
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    /*
    // Below is the old non-CoreData system
    [self.places addObject:newPlace];
    
    //Add new object to table with animation
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.places count] - 1) inSection:0];
    [self.placesTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    */
     
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)placeDetailControllerDelegateDidClose:(PlaceDetailController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)placeDetailcontrollerDelegateDidEdit:(PlaceDetailController *)controller editedPlace:(Place *)editedPlace {
    
    //NSIndexPath *path = [NSIndexPath indexPathForRow:[_places indexOfObject:editedPlace] inSection:0];
    
    //PlaceCell *cell = (PlaceCell *)[self.tableView cellForRowAtIndexPath:path];
    
    //[self updateCellContents:cell fromPlace:editedPlace];
    //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    //[self.placesTableView reloadData];
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return 1;
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return [self.places count];
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceCell" forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    cell.placeName.textColor = [UIColor whiteColor];
    cell.placeDatetime.textColor = [UIColor whiteColor];
    cell.placeImage.alpha = 1;
    
    return cell;

    
    /*
    PlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceCell"];
    Place *place = (self.places)[indexPath.row];
    
    [self updateCellContents:cell fromPlace:place];
    
    return cell;
     */
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    PlaceCell *placeCell = (PlaceCell *)cell;
    
    ManagedPlace *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    placeCell.placeName.text = object.placeName;
    placeCell.placeDatetime.text = [_dateFormatter stringFromDate:object.placeDate];
    placeCell.placeImage.image = [UIImage imageWithData:object.placeImage];
    
    // Rounded image with border
    CALayer *imageLayer = [placeCell.placeImage layer];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setCornerRadius:25.0];
    
    if (object.placeImage == nil) {
        [imageLayer setBorderWidth:0.5];
        [imageLayer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
}

// Old method for updating the table with non-CD data
- (void) updateCellContents:(PlaceCell *) ofCell fromPlace: (Place*) newPlace {
    ofCell.placeName.text = newPlace.placeName;
    ofCell.placeDatetime.text = [_dateFormatter stringFromDate:newPlace.placeDatetime];
    ofCell.placeImage.image = newPlace.placeImage;
    
    // Rounded image with border
    CALayer *imageLayer = [ofCell.placeImage layer];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setCornerRadius:25.0];
    
    [imageLayer setBorderWidth:0.5];
    [imageLayer setBorderColor:[[UIColor lightGrayColor] CGColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectedIndex != NSNotFound) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        PlaceDetailController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceDetail"];
        controller.delegate = self;

        NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        [controller setManagedPlace:object];
        
        /*
        _selectedIndex = indexPath.row;
        
        controller.place = [_places objectAtIndex:_selectedIndex];
        controller.delegate = self;
         */
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"NewPlace"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        NewPlaceController *newPlaceViewController = [navigationController viewControllers][0];
        newPlaceViewController.delegate = self;
    } else if([segue.identifier isEqualToString:@"PlaceDetail"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        PlaceDetailController *placeDetailController = [navigationController viewControllers][0];
        placeDetailController.delegate = self;
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        [placeDetailController setManagedPlace:object];
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ManagedPlace" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"placeDate" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */


@end
