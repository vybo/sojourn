//
//  PlacesViewController.m
//  Sojourn
//
//  Created by Dan Vybiral on 15/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import "FoldersViewController.h"
#import "Place.h"
#import "Folder.h"
#import "FolderCell.h"

@interface FoldersViewController ()
@property (strong, nonatomic) IBOutlet UITableView *foldersTableView;

@end

@implementation FoldersViewController

//@synthesize managedObjectContext;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)newPlaceControllerDelegateDidCancel:(NewPlaceController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)newPlaceControllerDelegateDidSave:(NewPlaceController *)controller didAddPlace:(Place *)newPlace
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.folders count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FolderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FolderCell"];
    
    Folder *folder = (self.folders)[indexPath.row];
    cell.folderHeader.text = folder.folderHeader;
    cell.folderTitle.text = folder.folderTitle;
    cell.folderSubtitle.text = folder.folderSubtitle;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"NewPlace"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        NewPlaceController *newPlaceViewController = [navigationController viewControllers][0];
        newPlaceViewController.delegate = self;
    }
}

@end
