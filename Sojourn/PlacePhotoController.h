//
//  PlacePhotoController.h
//  Sojourn
//
//  Created by student on 30/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacePhotoController : UIViewController

@property (weak, nonatomic) UIImage* placeImage;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
