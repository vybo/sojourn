//
//  PlacesViewController.h
//  Sojourn
//
//  Created by Dan Vybíral on 17/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewPlaceController.h"
#import "PlaceDetailController.h"
@import CoreData;

@interface PlacesViewController : UITableViewController <NewPlaceControllerDelegate, PlaceDetailControllerDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSMutableArray *places;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, assign) CGFloat screenHeight;

@property (strong, nonatomic) IBOutlet UITableView *placeTableView;

@end
