//
//  Folder.h
//  Sojourn
//
//  Created by Dan Vybíral on 18/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Folder : NSObject

@property (strong, nonatomic) NSString *folderHeader;
@property (strong, nonatomic) NSString *folderTitle;
@property (strong, nonatomic) NSString *folderSubtitle;
@property (strong, nonatomic) NSMutableArray *places;

@end
