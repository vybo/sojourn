//
//  Folder.m
//  Sojourn
//
//  Created by Dan Vybíral on 18/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import "Folder.h"

@implementation Folder

-(id) init {
    self = [super init];
    
    if (self) {
        _places = [[NSMutableArray alloc] init];
    }
    
    return self;
}

@end
