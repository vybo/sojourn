//
//  Place.h
//  Sojourn
//
//  Created by Dan Vybiral on 14/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@import UIKit;

@interface Place : NSObject

@property (nonatomic, strong) NSString* placeName;
@property (nonatomic, strong) NSString* placeDescription;
@property (nonatomic, strong) NSDate* placeDatetime;
@property (nonatomic, strong) CLLocation* placeLocation;
@property (nonatomic, strong) UIImage* placeImage;

@end
