//
//  NewPlaceController.h
//  Sojourn
//
//  Created by Dan Vybíral on 17/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
@import MapKit;

@class NewPlaceController;

@protocol NewPlaceControllerDelegate <NSObject>

- (void)newPlaceControllerDelegateDidCancel:(NewPlaceController *)controller;
- (void)newPlaceControllerDelegateDidSave:(NewPlaceController *)controller didAddPlace: (Place *)newPlace;
- (id)newPlaceControllerDelegateProvideManagedPlaceToEdit;
                                           
@end

@interface NewPlaceController : UIViewController <UITextFieldDelegate, CLLocationManagerDelegate, MKMapViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) id <NewPlaceControllerDelegate> delegate;
@property (nonatomic, weak) Place *place;

@property (weak, nonatomic) IBOutlet UIImageView *placePhoto;
@property (weak, nonatomic) IBOutlet UITextField *placeNameField;
@property (weak, nonatomic) IBOutlet UITextField *placeDescriptionField;
@property (weak, nonatomic) IBOutlet UITextField *placeTimeVisited;
@property (strong, nonatomic) NSDate *placeTime;
@property (strong, nonatomic) CLLocation *placeLocation;


- (BOOL)textFieldShouldClear:(UITextField *)textField;

@end
