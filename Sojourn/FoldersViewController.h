//
//  PlacesViewController.h
//  Sojourn
//
//  Created by Dan Vybiral on 15/03/15.
//  Copyright (c) 2015 Dan Vybiral. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewPlaceController.h"

@interface FoldersViewController : UITableViewController <NewPlaceControllerDelegate>

//@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, strong) NSMutableArray *folders;

@end
